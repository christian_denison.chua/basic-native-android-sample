package com.example.sampleapplication

import android.annotation.SuppressLint
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.TextView
import android.widget.Toast
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import butterknife.BindView
import butterknife.ButterKnife
import com.example.sampleapplication.databinding.ActivityMainBinding
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {

    private lateinit var binding: ActivityMainBinding

    private var listTasks: MutableList<String> = mutableListOf()

    private var x : String? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        //ViewBinding
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)

        init()
    }

    private fun init() {
        setSupportActionBar(binding.toolbarMain)
        supportActionBar?.title = "My To-Do List"

        binding.buttonAddTask.setOnClickListener {
            if (listTasks.isNullOrEmpty()) {
                setUpRecyclerView(binding.editTextTask.text.toString())
            } else {
                addTask(binding.editTextTask.text.toString())
            }
            binding.editTextTask.setText("")
        }
    }

    private fun setUpRecyclerView(task: String) {
        listTasks.add(task)
        binding.recyclerViewTasks.apply {
            adapter = SampleRecyclerViewAdapter(list = listTasks, onClickListener = { stringItem, position -> onItemClicked(stringItem, position) })
            layoutManager = GridLayoutManager(context,  2, RecyclerView.VERTICAL, false)
        }
    }

    private fun addTask(task: String) {
        listTasks.add(task)
        binding.recyclerViewTasks.adapter?.notifyDataSetChanged()
    }

    private fun onItemClicked(item: String, position: Int) {
//        listTasks[position] = "UPDATED VALUE: $item-$item"
//        binding.recyclerViewTasks.adapter?.notifyItemChanged(position)

        val intent = Intent(this, TaskDetailsActivity::class.java)
        intent.putExtra("task", item)
        startActivity(intent)
    }
}