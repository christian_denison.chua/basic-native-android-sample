package com.example.sampleapplication

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.sampleapplication.databinding.ItemTaskBinding

class SampleRecyclerViewAdapter(
    private val list: List<String>,
    private val onClickListener: (String, Int) -> Unit
    ) : RecyclerView.Adapter<SampleRecyclerViewAdapter.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val binding = ItemTaskBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return ViewHolder(binding)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.binding.textViewItemTask.text = list[position]
        holder.binding.constraintLayoutItemTaskContainer.setOnClickListener {
            onClickListener(list[position], position)
        }
    }

    override fun getItemCount() = list.size

    class ViewHolder(val binding: ItemTaskBinding) : RecyclerView.ViewHolder(binding.root)
}